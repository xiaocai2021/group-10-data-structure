package keshe01;

import keshe01.means;

import java.io.*;
import java.util.Scanner;

public class inface {
    private static String file = "C:\\Users\\SWCC1\\Desktop\\数据结构1\\group-10-data-structure\\group10\\src\\keshe01\\file.txt";

    public static void main(String[] args) throws IOException {
        view();
    }

    private static void view() throws IOException {
        Scanner scan = new Scanner(System.in);
        System.out.println("                   *用户主界面*                   ");
        System.out.println("************************************************");
        System.out.println("                 1、查询字符串个数");
        System.out.println("                   2、增加字符串");
        System.out.println("                   3、删除字符串");
        System.out.println("                   4、修改字符串");
        System.out.println("                   5、返回主界面");
        System.out.println("                   6、查看当前字符串");
        System.out.println("                 输入需要进行的操作！！！");
        System.out.println("***********************************************");
        int x = scan.nextInt();
        System.out.println();//跳转查询字符串
        switch (x) {
            case 1:
                seek();           //查
                break;
            case 2:
                inster();        //增
                break;
            case 3:
                delete();        //删
                break;
            case 4:
                change();        //改
                break;
            case 5:
                view();          //主界面
                break;
            case 6:
                putresout();     //当前文本
                break;
            case 7:
                return;
            default:
        }
    }

    private static void seek() throws IOException {
        Scanner scan = new Scanner(System.in);
        String x = String.valueOf(read(file));//SeqString s = new SeqString("abbcdeffcnvfgggnjvzffgggggghaaaadfsdgsddabaabcdeabaaabbbcabcdfgaaffegecdh");
        System.out.println("****************************************");
        means s = new means(x);
        System.out.println("输出要查询的子串：");
        String ch1 = scan.next();
        means ch = new means(ch1);
        System.out.println("子串的数目：" + s.StringCount(ch));//输出子串的个数
        System.out.println("****************************************");//  write("ch", String.valueOf(ch));
        write(file, String.valueOf(s));
        view();
    }

    private static void change() throws IOException {
        Scanner scan = new Scanner(System.in);
        String x = String.valueOf(read(file));
        //SeqString s = new SeqString("avhvhyjtvhnbmjhbhm");
        System.out.println("*****************************");
        System.out.println("输入要修改的字符：");
        String ch1 = scan.next();
        means aa = new means(x);
        means ch = new means(ch1);
        System.out.println("修改之后的字符：" + aa.replace(0, aa, ch));
        System.out.println("*****************************");
        write(file, String.valueOf(aa.replace(0, aa, ch)));
        view();
    }

    private static void delete() throws IOException {
        Scanner scan = new Scanner(System.in);
        String x = String.valueOf(read(file));
//        SeqString s = new SeqString("abbcdeffcnvfgggnjvzffgggggghaaaadfsdgsddabaabcdeabaaabbbcabcdfgaaffegecdh");
        System.out.println("****************************************");
        System.out.println("输入要删除的字符：");
        means aa = new means(x);
        String ch1 = scan.next();
        means ch = new means(ch1);
        System.out.println("删除后的字符串：" + aa.delete(ch));
        write(file, String.valueOf(aa.delete(ch)));
        System.out.println("删除指定位置的字符串：");
        System.out.println("输入删除的两个位置值：");
        int a = scan.nextInt();
        int b = scan.nextInt();
        System.out.println("删除指定位置后：" + aa.delete(a, b));
        System.out.println("****************************************");
        write(file, String.valueOf(aa.delete(a, b)));
        view();
    }

    private static void inster() throws IOException {
        Scanner scan = new Scanner(System.in);
        String x = String.valueOf(read(file));
        //   SeqString s=new SeqString("abbcdeffcnvfgggnjvzffgggggghaaaadfsdgsddabaabcdeabaaabbbcabcdfgaaffegecdh");
        System.out.println("*******************************");
        System.out.println("输出要增加的子串：");
        means aa = new means(x);
        String ch1 = scan.next();
        means ch = new means(ch1);
        System.out.println("请输入从第几个位置插入：");
        int d = scan.nextInt();
        System.out.println("增加后的子串:" + aa.insert(d, ch));//输出子串的个数
        System.out.println("*******************************");
        write(file, String.valueOf(aa.insert(0, ch)));
        view();
    }

    public static void putresout() throws IOException {//输出当前字符串
        put(file);
        view();
    }

    public static String put(String fileName) throws IOException {
        File file = new File(fileName);
        InputStream in = null;
        try {
            in = new FileInputStream(file);
            int temp;
            while ((temp = in.read()) != -1) {
                System.out.print((char) (temp));
            }
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
            return fileName;
        }
        return "当前文本为：" + fileName;
    }

    public static StringBuffer read(String strFile) throws IOException {
        StringBuffer strSb = new StringBuffer();
        InputStreamReader inStrR = new InputStreamReader(new FileInputStream(strFile), "UTF-8");
        BufferedReader br = new BufferedReader(inStrR);
        String line = br.readLine();
        while (line != null) {
            strSb.append(line).append("\r\n");
            line = br.readLine();
        }
        return strSb;
    }

    public static void write(String fileName, String s) throws IOException {
        File f1 = new File(fileName);
        OutputStream out = null;
        BufferedWriter bw = null;
        if (f1.exists()) {
            out = new FileOutputStream(f1);
            bw = new BufferedWriter(new OutputStreamWriter(out, "utf-8"));
            bw.write(s);
            bw.flush();
            bw.close();
        } else {
            System.out.println("文件不存在");
        }
    }
}
