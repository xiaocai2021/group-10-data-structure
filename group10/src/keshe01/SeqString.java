package keshe01;

public class SeqString implements IString{
    private char[] strvalue;
    private int curlen;
    private SeqString curstr;

    //构造空串
    public SeqString(){
        strvalue = new char[0];
        curlen = 0;
    }
    //以字符串常量构造串
    public SeqString(String str) {
        char[] tempchararray = str.toCharArray();
        strvalue = tempchararray;
        curlen = tempchararray.length;
    }
    //以字符数组构造串
    public SeqString(char[] value) {
        this.strvalue = new char[value.length];
        for (int i = 0; i < value.length; i++) {
            this.strvalue[i] = value[i];
        }
        curlen = value.length;
    }

    @Override
    public void clear() {
        this.curlen = 0;
    }//将一个已经存在的串置成空串

    @Override
    public boolean isEmpty() {
        return curlen == 0;
    }//判断当前串是否为空，为空则返回true，否则返回false

    @Override
    public int length() {
        return curlen;
    } //返回字符串长度

    @Override
    //返回字符串中序号为index的字符
    public char charAt(int index) {
        if (( index < 0 ) || ( index >= curlen )) {
            throw new StringIndexOutOfBoundsException(index);
        }
        return strvalue[index];
    }
    //扩充容量，参数指定最小容量
    public void allocate(int newCapacity) {
        char[] temp = strvalue;
        strvalue = new char[newCapacity];
        for (int i = 0; i < temp.length; i++) {
            strvalue[i] = temp[i];
        }
    }
    @Override
    //返回串中序号从begin至end-1的子串
    public IString substring(int begin, int end) {
        if (begin < 0) {
            throw new StringIndexOutOfBoundsException("起始位置不能小于0");
        }
        if (end > curlen) {
            throw new StringIndexOutOfBoundsException("结束位置不能大于串的当前长度：" + curlen);
        }
        if (begin > end) {
            throw new StringIndexOutOfBoundsException("开始位置不能大于结束位置");
        }
        if (begin == 0 && end == curlen) {
            return this;
        } else {
            char[] buffer = new char[end - begin];
            for (int i = 0; i < buffer.length; i++) //复制子串
            {
                buffer[i] = this.strvalue[i + begin];
            }
            return new SeqString(buffer);
        }
    }
    public IString substring(int begin)   //返回序号从begin至尾串的子串
    {
        return substring(begin,strvalue.length);
    }
    @Override

    public IString insert(int offset, IString str)  //在当前串的第offset个字符之前插入串str，0<=offset<=curlen
    {
        if (( offset < 0 ) || ( offset > this.curlen )) {
            throw new StringIndexOutOfBoundsException("插入位置不合法");
        }
        int len = str.length();
        int newCount = this.curlen + len;
        if (newCount > strvalue.length) {
            allocate(newCount);             // 插入空间不足，需扩充容量
        }
        for (int i = this.curlen - 1; i >= offset; i--) {
            strvalue[len + i] = strvalue[i];  //从offset开始向后移动len个字符
        }
        for (int i = 0; i < len; i++) {
            strvalue[offset + i] = str.charAt(i);   //复制字符串str
        }
        this.curlen = newCount;
        return this;
    }
    @Override

    public IString delete(int begin, int end) //删除从begin到end-1的子串， 0≤begin≤length()-1，1≤end≤length()。
    {
        if (begin < 0) {
            throw new StringIndexOutOfBoundsException("起始位置不能小于0");
        }
        if (end > curlen) {
            throw new StringIndexOutOfBoundsException("结束位置不能大于串的当前长度" + curlen);
        }
        if (begin > end) {
            throw new StringIndexOutOfBoundsException("开始位置不能大于结束位置");
        }
        for (int i = 0; i < curlen - end; i++) //从end开始至串尾的子串向前移动到从begin开始的位置
        {
            strvalue[begin + i] = strvalue[end + i];
        }
        curlen = curlen - ( end - begin );
        return this;
    }
    @Override

    public IString concat(IString str) //添加字符串到队尾
    {
        return insert(curlen,str);
    }
    @Override

    public int compaereTo(SeqString str) //当前串与目标串比较
    {
        int len1 = curlen;
        int len2 = str.curlen;
        int n = Math.min(len1, len2);
        char[] s1 = strvalue;
        char[] s2 = str.strvalue;
        int k = 0;
        while (k < n) {
            char ch1 = s1[k];
            char ch2 = s2[k];
            if (ch1 != ch2) {
                return ch1 - ch2;
            }
            k++;
        }
        return len1 - len2;
    }



    public String toString() {
        return new String(strvalue, 0, curlen);   //以字符数组strvalue构造串
    }

    private int[] getNext(IString str) //计算模式串的next[]函数值
    {
        int[] next = new int[str.length()];
        int j = 1;
        int k = 0;
        next[0] = -1;
        next[1] = 0;
        while (j < str.length() - 1) {
            if (str.charAt(j) == str.charAt(k)) {
                next[j + 1] = k + 1;
                j++;
                k++;
            } else if (k == 0) {
                next[j + 1] = 0;
                j++;
            } else {
                k = next[k];
            }
        }

        return ( next );
    }
    //若当前串中存在和str相同的子串，则返回模式串str在主串中从第start字符开始的第一次出现位置，否则返回-1
    public int indexOf(IString t, int start) {
        return indexOfKMP(t, start);
    }
    // 模式匹配的Brute-Force 算法
    //返回模式串t在主串中从start开始的第一次匹配位置，匹配失败时返回－1。
    @Override
    public int indexOfBF(IString str, int begin) {
        if (this != null && str != null && str.length() > 0 && this.length() >= str.length()) {  //当主串比模式串长时进行比较
            int slen, tlen, i = begin, j = 0;    //i表示主串中某个子串的序号
            slen = this.length();
            tlen = str.length();
            while ((i < slen) && (j < tlen)) {
                if (this.charAt(i) == str.charAt(j)) //j为模式串当前字符的下标
                {
                    i++;
                    j++;
                } //继续比较后续字符
                else {
                    i = i - j + 1;        //继续比较主串中的下一个子串
                    j = 0;                //模式串下标退回到0
                }
            }
            if (j >= str.length()) //一次匹配结束，匹配成功
            {
                return i - tlen;         //返回子串序号
            } else {
                return -1;
            }
        }
        return -1;                     //匹配失败时返回-1
    }

    //KMP算法
    @Override
    public int indexOfKMP(IString str, int begin) {
        int[] next = getNext(str);
        int i = begin;
        int j = 0;
        while (i < this.length() && j < str.length()) {
            if (j == -1 || this.charAt(i) == str.charAt(j)) {
                i++;
                j++;
            } else {
                j = next[j];
            }
        }
        if (j < str.length()) {
            return -1;
        } else {
            return ( i - str.length() );
        }
    }
    public int StringCount(SeqString str)  //字符或字符串出现次数
    {
        SeqString source = this;
        int count=0,begin=0;
        int index;
        while((index = source.indexOfBF(str,begin))!=-1){
            count++;
            begin=index+str.length();
        }
        return count;
    }
    public SeqString replace(int begin,SeqString s1,SeqString s2)//用一个新的字符串替换原来的字符串
    {   //begin int 开始位置，s1 String 原始字符串，s2 String 目标字符串
        if(s1 == null || s2 == null){
            return null;
        }
        SeqString ss=new SeqString("");
        SeqString source =this;
        int index =-1;
        while((index=source.indexOf(s1,begin))!=-1){
            ss.concat(source.substring(0,index));
            ss.concat(s2);
            source=(SeqString)source.substring(index + s1.length());
        }
        ss.concat(source);
        return ss;
    }
}
