package keshe01;

public interface IString {

    public void clear();//串置空

    public boolean isEmpty();//串判空

    public int length();//串长

    public char charAt(int index);//取字符

    public IString substring(int begin, int end);

    public IString insert(int offset, IString str);//插入

    public IString delete(int begin, int end);//删除

    public IString concat(IString str);//连接

    public int compaereTo(SeqString str);//比较

    // 模式匹配的Brute-Force 算法
    //返回模式串t在主串中从start开始的第一次匹配位置，匹配失败时返回－1。
    int indexOfBF(IString str, int begin);

    public int indexOfKMP(IString str, int begin);//字串定位
}
